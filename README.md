To build this site, install the `hugo` package.

Then, after making some changes, you can run the `hugo` command in
the top-level directory to generate the new static pages.

Then simply add all the new files to a git commit and push.
