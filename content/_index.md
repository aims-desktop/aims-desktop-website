---
date: 2017-04-10T11:27:13+01:00
title: AIMS Desktop
type: index
weight: 0
---

## Announcing AIMS Desktop 2024.1

The AIMS Desktop team is proud to announce AIMS Desktop 2024.1. This release is based on Debian 12.

This site contains the <a href="download">download links</a>, <a href="getting-started">installation guide</a> and other resources for AIMS Desktop.

## Announcing AIMS Desktop 2022.1

The AIMS Desktop team is proud to announce AIMS Desktop 2022.1. This release is based on Debian 11, and is released on 1 September 2021.

This site contains the <a href="download">download links</a>, <a href="getting-started">installation guide</a> and other resources for AIMS Desktop.

AIMS Desktop is a collection of software that allows you to do mathematics and science on your computer easily and efficiently. You need no exising software on your computer, AIMS Desktop installs an entire operating system that you can install standalone, or dual boot with Microsoft Windows or Apple macOS.

It is maintained by AIMS (The African Institute for Mathematical Sciences), a pan-African network of centres of excellence enabling Africa’s talented students to become innovators driving the continent’s scientific, educational and economic self-sufficiency.

AIMS Desktop is free for anyone to use for educational, non-profit or commercial purposes. (See: <a href="license">Software Licences</a> for more information)

Refer to the <a href="getting-started">installation guide</a> for instructions how to get AIMS Desktop up and running.


## Announcing AIMS Desktop 2020.1

The AIMS Desktop team is proud to announce AIMS Desktop 2020.1. This release is based on Debian 10, and through some careful planning, we're releasing it on the same day as the official Debian 10 release on Saturday, 5 July 2020.

This site contains the <a href="download">download links</a>, <a href="getting-started">installation guide</a> and other resources for AIMS Desktop. We plan to continually expand it.

AIMS Desktop is a collection of software that allows you to do mathematics and science on your computer easily and efficiently. You need no exising software on your computer, AIMS Desktop installs an entire operating system that you can install standalone, or dual boot with Microsoft Windows or Apple macOS.

It is maintained by AIMS (The African Institute for Mathematical Sciences), a pan-African network of centres of excellence enabling Africa’s talented students to become innovators driving the continent’s scientific, educational and economic self-sufficiency.

AIMS Desktop is free for anyone to use for educational, non-profit or commercial purposes. (See: <a href="license">Software Licences</a> for more information)

Refer to the <a href="getting-started">installation guide</a> for instructions how to get AIMS Desktop up and running.

## Announcing AIMS Desktop 2017.1

The AIMS Desktop team is proud to announce AIMS Desktop 2017.1. This release is based on Debian 9, and through some careful planning, we're releasing it on the same day as the official Debian 9 release on Saturday, 17 June 2017.

![Students testing release candidate](images/rc-install.jpg)
<i>Students testing the release candidate at AIMS South Africa</i>

To see what's new and for a list of known issues, see the <a href="../iso/aims-desktop-2017.1-release-announcement.txt">release announcement</a>.

This site contains the <a href="download">download links</a>, <a href="getting-started">installation guide</a> and other resources for AIMS Desktop. We plan to continually expand it.

## Welcome to AIMS Desktop

![AIMS Desktop Picture](/images/screen.jpg)

AIMS Desktop is a collection of software that allows you to do mathematics and science on your computer easily and efficiently. You need no exising software on your computer, AIMS Desktop installs an entire operating system that you can install standalone, or dual boot with Microsoft Windows or Apple macOS.

It is maintained by AIMS (The African Institute for Mathematical Sciences), a pan-African network of centres of excellence enabling Africa’s talented students to become innovators driving the continent’s scientific, educational and economic self-sufficiency.

AIMS Desktop is free for anyone to use for educational, non-profit or commercial purposes. (See: <a href="license">Software Licences</a> for more information)

Refer to the <a href="getting-started">installation guide</a> for instructions how to get AIMS Desktop up and running.

## Acknowledgements

The AIMS Desktop project is maintained by <a href="https://aims.ac.za">AIMS South Africa</a> and
contributors.

### Meet the team.

<span> <img style="float: left; padding-right: 10px;" src="/images/faces/pipedream.jpg" alt="Photo of Jan" /> <h3> Jan Groenewald </h3>
Jan is the IT Manager at AIMS South Africa and have worked on various forms of AIMS Desktop since 2003.
He conducted the first deployment of desktops at AIMS using Debian Sarge
to Sagemath upstream packaging. Jan maintains AIMS meta-packages and focusses on science software and
software used for academic purposes in the system.
</span>

<span> <img style="float: left; padding-right: 10px;" src="/images/faces/highvoltage.jpg" alt="Photo of Jonathan" /> <h3> Jonathan Carter </h3>
Jonathan works part time at AIMS South Africa and maintains the PPA infrastructure, build scripts,
installer packages, this site and settings packages included in AIMS Desktop. Jonathan is currently
a Debian Developer in the Debian project. <br /> <br />
</span>

### External projects

AIMS Desktop is built on the shoulders of giants. Here are only some of the projects that it is built on:

 * <a href="https://debian.org">Debian project</a> - A large association of developers who build Debian, a general-purpose operating system environment made up of a large collection of free software.
 * <a href="https://en.wikipedia.org/wiki/Linux_kernel">Linux Kernel</a> - The heart of the system that handles all hardware and drivers.
 * <a href="https://calamares.io/about/">Calamares</a> - An independent graphical installer framework.
 * <a href="https://gnome.org">GNOME Desktop Environment</a> - The default desktop environment we ship with AIMS Desktop.
 * <a href="https://jupyter.org/">Jupyter Notebook</a> - A web application that allows you to create and share documents that contain live code, equations, visualizations, and explanatory text.
 * <a href="https://www.python.org/">Python</a> - A powerful programming language that's easy to learn, many programs shipped with AIMS Desktop is written in Python.
 * <a href="http://www.sagemath.org/">Sagemath</a> - Mathematics software that builds on many existing projects including NumPy, SciPy, matplotlib, Sympy, Maxima, GAP, FLINT and R.
 * <a href="https://en.wikipedia.org/wiki/GNU_Octave">GNU Octave</a> - A (mostly Matlab Ⓡ compatible) high-level language, primarily intended for numerical computations.
 * <a href="https://en.wikipedia.org/wiki/TeXstudio">Tex studio</a>
 * <a href="https://en.wikipedia.org/wiki/Firefox">Firefox</a> - A free web browser that aims to protect you on-line.

<!--
### Kafui Odzanga Dake

![Photo of Kafui](/images/faces/kafui.jpg)

Kafui is the IT Manager at AIMS Ghana and have worked on package selection as well as QA on AIMS
Desktop.
-->
