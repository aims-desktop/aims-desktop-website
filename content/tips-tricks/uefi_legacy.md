---
date: 2020-12-03T12:33:00+0200
title: UEFI vs Legacy Boot
weight: 10
---

UEFI and Legacy Boot are two different ways of booting a computer. Most PCs and
laptops released after 2007 have UEFI installed. UEFI stands for Unified
Extendable Firmware Interface and was designed to replace BIOS (Legacy Boot).

A major difference between UEFI and Legacy Boot is Secure Boot. This is a
feature of UEFI that prevents loading an operating system not signed by the
manufacturer's key. AIMS desktop is not signed by this key and so Secure Boot
must be disabled before AIMS Desktop can be installed.

Secure Boot is managed by the UEFI settings. From Windows 10, it can be
disabled by going to *Settings -> Updates & security -> Recovery* and selecting
*Restart now*. This restart the PC into a recovery mode with various options.
Select *Troubleshoot -> Advanced options -> UEFI Firmware Settings -> Restart*.
This will boot into the UEFI Settings, where Secure Boot can be disabled. The
exact location of the option differs between manufacturers, but it is generally
found in the Boot section.

From Linux machines, there is usually either a Grub menu entry or a keyboard
shortcut that can be pressed while the PC is booting that will access the
UEFI Settings.
