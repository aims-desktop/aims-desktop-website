---
date: 2016-03-09T19:56:50+01:00
title: Tips and Tricks
weight: 20
---

## Managing software

Being based on Debian, AIMS Desktop uses the APT (Advanced Package Tool) to manage software.

Software and its dependencies on AIMS Desktop is managed via packages, for each program on
your computer, there is at least one package. Packages can be installed, removed or upgraded.

Here we will show you how to manage software using APT in the command line. APT needs root
(administrator) privileges, so you need to run it using the sudo command.

For example, if you want to install the program "meld", you would use `apt install`:

```none
$ sudo apt install meld
The following NEW packages will be installed:
  meld
Do you want to continue? [Y/n] y
```

If you would like to remove that package, you can use `apt remove`:

```none
$ sudo apt remove meld
The following packages will be REMOVED:
  meld
Do you want to continue? [Y/n] y
```

APT's remove tool plays it safe, and doesn't automatically remove configuration
files or data that may have been created for a package in the system. If you'd
like to remove that as well, you can use the purge command. This does not affect
any data in your home directory.

```none
$ sudo apt purge meld
The following packages will be REMOVED:
  meld*
Do you want to continue? [Y/n] y
```
You will notice an asterisk next to the package name, this indicates that the
package will be purged, as apposed to a normal removal.

Security updates and other important software updates happen regularly. To update
the list of the available packages for your system, you can run `apt update`. To
install any available upgrades, you can run `apt upgrade`.

```none
$ sudo apt update
Hit:1 http://http.debian.net/debian stretch InRelease
Get:1 http://security.debian.org/debian-security stretch/updates InRelease [62.9 kB]
...
$ sudo apt upgrade
The following packages were automatically installed and are no longer required:
  libnfsidmap2 libtirpc1
  Use 'sudo apt autoremove' to remove them.
The following packages will be upgraded:
  busybox
Do you want to continue? [Y/n] y
```

In the example above, apt points out that two of the installed packages are no longer
required. They might have been installed as dependencies of packages that have been
previously removed. To remove them, use the `apt autoremove` command:

```none
The following packages will be REMOVED:
  libnfsidmap2 libtirpc1
Do you want to continue? [Y/n]
```

{{< note title="Tip: GNOME Software" >}}
GNOME Software is a graphical front-end that supports APT. It makes it easier to
find and install software if you're still new to the system.

<div style="text-align: center; margin-top: 1em;">
<img src="/images/tips-tricks/gnome-software.png" alt="" />
</div>
{{< /note >}}

## Connecting to remote servers via SSH

**Introduction:** Servers that you normally ssh into can be accessed through
the Nautilus file manager. This allows you browse your remote files, edit them
and copy to and from the remote server.

**Step 1:** Open Nautilus/Files
<div style="text-align: center; margin-top: 1em;">
<img src="/images/tips-tricks/2017.01-nautilus-ssh-1.png" alt="" />
</div>

**Step 2:** In the location pane, click on *Other Locations*.
<div style="text-align: center; margin-top: 1em;">
<img src="/images/tips-tricks/2017.01-nautilus-ssh-2.png" alt="" />
</div>

**Step 3:** At the bottom there is a field to *Connect to Server*.
<div style="text-align: center; margin-top: 1em;">
<img src="/images/tips-tricks/2017.01-nautilus-ssh-3.png" alt="" />
</div>

**Step 4:** In this field enter `ssh://<user>:<server>`, where `<user>` is your
username and `<server>` is the address of the server you are connecting to.
Replace `ssh` with `sftp` if you wish to connect to a SFTP server.

**Step 5:** This will prompt you for a password if necessary and mount the
location. You will see the server in the left panel and a listing of files on
the right.

**Step 6:** If you wish to bookmark this server for future use, right-click on
the server in the location pane and select *Add Bookmark*.
<div style="text-align: center; margin-top: 1em;">
<img src="/images/tips-tricks/2017.01-nautilus-ssh-4.png" alt="" />
</div>

## Changing your password

### GNOME Desktop

**Step 1:** Open the Activities overview and start typing *Users*.
<div style="text-align: center; margin-top: 1em;">
<img src="/images/tips-tricks/2017.01-change-password-1.png" alt="" />
</div>

**Step 2:** Click *Users* to open the panel.
<div style="text-align: center; margin-top: 1em;">
<img src="/images/tips-tricks/2017.01-change-password-2.png" alt="" />
</div>

**Step 3:** Click the label next to *Password*.

**Step 4:** Enter your current password, then a new password.

**Step 5:** Enter your new password again in the *Verify New Password* field.
You can press the gear icon to automatically generate a random password.
<div style="text-align: center; margin-top: 1em;">
<img src="/images/tips-tricks/2017.01-change-password-3.png" alt="" />
</div>

**Step 6:** Click *Change*.

### Command Line
**Step 1:** Open the terminal.
**Step 2:** Enter
```
passwd
```
and follow the prompts.

**Step 3:** Enter your current password, your new password and then confirm your
new password. You will not be able to see your password when you type it in.

{{< note title="How to choose a strong password" >}}
* Make your passwords easy enough for you to remember, but very difficult to for
others to guess. Your password guards your personal information, so you should
make sure it is a strong one.
* Use a mixture of upper-case and lower-case letters, numbers, symbols and
spaces in the password. A good way of doing this to the take the first letter of
each word and the grammar of a phrase you can remember.
* Make your password as long as possible. This makes it difficult for a person
or computer to guess it.
* Do not use one or two words from a dictionary in any language. These are what
programs use to try crack your password.
* Do not use any personal information such as a date, family member's name or a
pet.
* Never write your password down.
* Use different passwords for different things.
{{< /note >}}

## File Templates

In previous versions, you could right-click anywhere in the file manager and
create a new blank file. In the current version, this has been extended so that
you can create any kind of file as a template for a new file. This could be a
shell script, document, blank file or any other kind of document that you create
on a regular basis.

To do this, all you need to do is place new documents as template files in your
Templates directory.

<img alt="" width="100%" title="Templates directory in file browser"
src="/images/templates/templates-1.png" />

Then, those documents will be available when you right-click in the file
browser:

<img alt="" width="100%" title="Templates being accessed in file browser"
src="/images/templates/templates-2.png" />
