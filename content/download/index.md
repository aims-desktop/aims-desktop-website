---
date: 2017-06-05T15:08:11+02:00
title: Download
weight: 50
---

<b>Download location</b>

Download the current release: <a href="../iso/aims-desktop-2024.1-20231004-amd64.iso">aims-desktop-2024.1-20231004-amd64.iso</a>

This download is about 4GB large. If you have data limits on your current connection,
please take that into consideration before downloading.

<b>Validating the image</b>

SHA256 Checksum: `ba7d3e8ebbf11bd176d3b5c4cdb572bffe209a4b0b84d5728c00912d99a24c1b`

The checksum above is a SHA256 sum that you can verify in Linux or OSX using the following command on the iso file, for example:

```none
user@localhost$ sha256sum aims-desktop-2024.1-20231004-amd64.iso
```

If the checksum matches the sha256 checksum above, then your local file is undamaged.
There is also a command line sha256 tool for Microsoft Windows, which can be downloaded
<a href="http://www.labtestproject.com/files/win/sha256sum/sha256sum.exe">here.</a>

{{< note title="What's next?" >}}
Once you've downloaded the file above, you can proceed to the <a href="/getting-started">Getting Started</a> section that will guide you through the installation process.
{{< /note >}}

## Mirrors

We'll get the latest release updated on mirrors ASAP.

<!--
Mirrors are sites that keep a copy of our download location so that local users can download it much faster. Choosing a mirror closest to you should result in the fastest download times.

<ul>
<li><img src="../images/flags/flag_de_18.png" /> Germany - <a href="/iso/"> Primary international download site</a></li>
<li><img src="../images/flags/flag_sa_18.png" /> South Africa - <a href="http://ftp.sun.ac.za/ftp/iso-images/aims/"> Stellenbosch University</a></li>
</ul>

If you have bandwidth to spare and would like to host a copy of AIMS Desktop, you can sync via `
rsync://desktop.aims.ac.za/aims-desktop-iso/` and  <a href="mailto:help@aims.ac.za">email us</a> a link and we'll add it to this page.
-->
