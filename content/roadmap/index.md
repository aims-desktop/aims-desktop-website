---
date: 2017-04-12T13:08:11+03:00
title: Roadmap
weight: 30
---

AIMS Desktop follows the Debian releases and we aim to release a new major version along with
every major Debian release. We also aim to release at least an updated version with the beginning
of the academic intakes at AIMS (which happens in January and August every year). Since Debian releases
aren't entirely predictable, this means that there will typically be 3-4 AIMS Desktop releases during
a Debian release cycle depending on Debian's release dates.

## 2017.1 (based on Debian 9)

 * This is our first public, Debian-based release. In the past, AIMS Desktop was based on Ubuntu.
   We changed to Debian for its
   <a href="https://www.debian.org/social_contract">friendlier licensing conditions</a>.
   In this release, we aim to match the features and usability of previous AIMS Desktop releases.
 * This release will feature Sagemath and Jupyter Notebook installed directly from the Debian archives.
 * The goal is also to have this release available on this site, world-wide available for free
   to anyone who would like to use it.
 * We now use GRUB For both legacy and UEFI boot, obsoleting isolinux for legacy booting.
 * As of 17 June 2017, this is now released. See the release announcement for further details.

## 2017.2 (based on Debian 9)

 * First major update to 2017.1 based on user feedback.
 * Theming touch-ups.
 * Add software selection to the installer (Calamares netinstall module)
 * Add Howtos to the site.
 * Documentation: Add troubleshooting guide to the website
 * Documentation: Add advanced partitioning guide to the website
 * Website: Make offline copy of the website available in a package
 * Website: Add blog
 * More details to follow.

## 2018.1 (based on Debian 9)

 * We aim to have proper packaging for R Studio.
 * More details to follow.

## 2019.1 (based on Debian 10)

 * We aim to include the AIMS Desktop meta-packages in the Debian repositories so that we can build AIMS
   desktop entirely from the Debian repositories. The goal is that we buidl AIMS Desktop entirely
   from Debian packages and gain the status of a pure blend (https://www.debian.org/blends/).
 * More details to follow.
